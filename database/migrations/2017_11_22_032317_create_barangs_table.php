<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kodebrg',10);
            $table->string('namabrg',50);
            $table->integer('id_satuan');
            // $table->integer('id_satuan')->unsigned()->notnull();
            // $table->foreign('id_satuan')->references('id')->on('satuans');
            $table->integer('hargabrg');
            $table->timestamps();
        });

        DB::table('barangs')->insert(
            array(
                'kodebrg' => 'PK001',
                'namabrg' => 'Paku 3 cm',
                'id_satuan' => '2',
                'hargabrg' => '5000'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
