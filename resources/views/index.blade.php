@extends('layout.app')

@section('title','List Barang')

@section('body')

<button type="button" class="btn btn-primary btn-sm" onclick="location.href='barang/create'">Tambah Barang</button>
<table class="table table-striped table-hover table-bordered">
  <thead class="thead-dark">
    <tr>
      <th>No</th>
      <th>Kode Barang</th>
      <th>Nama Barang</th>
      <th>Satuan Barang</th>
      <th>Harga</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
  	@foreach ($barangs as $brg)
	    <tr>
	      <td>{{ $loop->iteration }}</td>
	      <td>{{ $brg->kodebrg }}</td>
	      <td>{{ $brg->namabrg }}</td>
	      <td>{{ $brg->satuan->satuan }}</td>
	      <td class="text-right">{{ number_format($brg->hargabrg) }}</td>
	      <td>
			<form action="/barang/{{ $brg->id }}" method="post" >
				{{ csrf_field() }}
  				{{ method_field('DELETE') }}
		      	<button type="button" class="btn btn-primary btn-sm" 
		      	onclick="location.href='/barang/{{ $brg->id }}/edit'" >Edit</button>
  				<button type="submit" class="btn btn-primary btn-sm">Delete</button>
			</form>
	      </td>
	    </tr>
  	@endforeach

  </tbody>
</table> 

@endsection